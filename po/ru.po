# Russian translation for crux.
# Copyright (C) 2003 Free Software Foundation, Inc.
# This file is distributed under the same license as the crux package.
# Dmitry G. Mastrukov <dmitry@taurussoft.org>, 2003.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: crux\n"
"POT-Creation-Date: 2003-01-10 10:45+0300\n"
"PO-Revision-Date: 2003-01-10 12:10+0300\n"
"Last-Translator: Dmitry G. Mastrukov <dmitry@taurussoft.org>\n"
"Language-Team: Russian <gnome-cyr@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: index.theme.in.h:1
msgid "Brought to you by gradients and the colour teal"
msgstr "Тема имитирует окрас чирка"

#: index.theme.in.h:2 icons/index.theme.in.h:1
msgid "Crux"
msgstr "Кракс"

#: icons/index.theme.in.h:2
msgid "The Crux icon theme designed by Arlo Rose"
msgstr "Тема \"Кракс\" создана Арло Роузом"
